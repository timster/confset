confset: A Distributed and Versioned Configuration Management System
====================================================================
Tim Lewis
---------


Introduction
------------



confset is a system to allow you to share your configuration files
across a number of machine and share configurations with others. It
uses a simple mechanism of symbolic links to embed the files in your
filesystem. This has the advantage of allowing you to perform local
updates of the files transparently and have all of your changes kept
in the versioned tree.

This documentation uses confset to refer both to the package as a
whole and a particular set of configuration files that are managed by
it. *Use confset to manage your own confset*.


Prerequisites
-------------

To use confset fully you will need the following:


* **Git** Any recent version should be fine - confset does not rely on anything
beyond the basic toplevel commandline git usage. 

* **Python** Currently versions >2.7 should work, although there is some attempt
to support earlier versions. CHECK



Installing confset
------------------

You should be able to download and install the system using the
command:


	git clone git@bitbucket.org:timster/confset.git LOCALDIR


where `localdir` is your choice of installation dir. To run just execute


      $LOCALDIR/conf/conf COMMAND --set mine


Add `$LOCALDIR/conf` to your `$PATH` so you can run conf directly from the
command line. The `--set` is optional, if not supplied it will just
use the first set defined in your `.confrc`.


Using confset
-------------

There are a number of ways to start using confset. 


##You may want to start to set up a shared confset amongst all of your
machines.


Start first by setting up your .confrc file to contain the basic info
of your set up. 

This is an example .confrc:

	{
		"mine": {

		"gitdir": "/home/you/git/etc",	
 	
		"gitremote": "https://you@bitbucket.org/hamster/conf.git", 
	
		"savedir": "/home/you/oldconfig", 
	
		}	
	}



* **mine** is the name you give to this particular set, this allows you to
manage more than one set of confs from a single user account.


* **gitdir** points to the local directory to which you have pulled the
git repo. You can just use this as a local directory that stores the
conf files, however. 


* **gitremote** tells conf where remotely you want to store your git
repository. You can bypass this and manage  your git setup
independantly if you wish - just set this to "".


* **savedir** is a holding directory for files that are replaced
from the confset. After running update or restore commands, check
this directory for old configs that have been replaced by the
operation. Before proceeding you should decide whether to keep then
and possibly merge them into the ones in the current confset.


You now need to initialise the local git store. You can do this from
within conf:


       :> conf init --set mine

This sets the directory "gitdir" up as a git store, and deposits a fresh
default confset configuration file in it, called gitdir/default.json.

You then need to edit this file to get the functionality that you want.

Example default.json file:

	{
		"confs":  [".bashrc",
	    ".fvwm2rc", 
	    ".i3", 
	    ".i3status.conf", 
	    ".xemacs", 
	    ".xinitrc", 
	    ".confrc",
	    ".python.py",
	    ".gconf/apps/gnome-terminal/profiles"
	   ],

	    "basedir": "/home/you", 

	     "harm": true,

	     }	     



This includes the following fields:


* **basedir** gives the base directory to which all files are
relative. Usually set to your home directory (as given by the python
call `expanduser("~")`, to which it defaults if left blank.

* **confs** defines the full list of files managed by the system. Note
that a file f1 will be linked into your filesystem at the point
`basedir/f1` . You can override this behaviour by giving the explicit
path in the form `["f1", "target_link"]`. Note that if the git tree contains a
whole directory of files that you need linked in it is only necessary
to specify the top level directory name here. The whole tree will then
be linked in from that directory.  

* **harm** Set to false confset will avoid performing destructive or
irreversible actions, it will merely inform of its actions. Set to
true to cause damage :)

* **verbose** Logging level, set to:

     1) Only errors
     2) Warnings
     3) Informs only of changes to your setup as they happen
     4) Information about everything it is doing.

Once you have added the confs you want preserved, run:

     :> conf refresh mine

to bring everything up to date. This will stash all of the files in
the git tree and commit them, by default it leaves git push and pull
functions to you.  It then links them up to the correct place
in your filesystem.

If you add a new file to the `default.json`  file rerun this command to
put it into action.


##You may know of an online confset you want to use locally on your
machine.


This is also the procedure you use when moving your configuration to a
fresh machine.

First, create a `.confrc` with the correct settings for the git
repository location, the local storage directory and the `savedir` for
preseving existing files. Assume your confset is named `mine`.

To pull down the files and initiase the local setup run:


   :> conf install mine


This will also link everything locally. It will break existing links,
but will keep a temporary copy of any replaced files in `savedir`.

Behind the scenes
-----------------

The main part of a local confset is the git repository stored in
`gitdir`. This tree can be managed entirely outside the confset
tools, and provided the git support within confset is disabled (using
`"gitdir": ""`)  can be managed using other versioning tools or
does not even need to be a versioned repository.

The second aspect is the symbolic links that tie these files to the
correct place in the filesystem, usually confined to the user's home
directory. These links are created and maintained using the confset tool. 

# Future Work

Will allow the use of hard links, 

Copy links as an alternatives to the standard symbolic ones.  Copy
links will just copy the file from the repository to the target
location, breaking the link between the local and versioned
instances. This is useful in situations where the default behaviour
will be to have a separate local version of the configuration file
that needs to be different on each deployed machine, but still
benefits from having a global reference version from which it is
constructed. 


Branching:
An alternative implementation mechanism for this concept
would use an explicit local branch for each machine.


