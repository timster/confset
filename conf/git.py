## Weak layer over git stuff
## Add: port to stronger standard git bindings

import os
from util import *


def init (gitdir="."):

    return dolocal(gitdir, lambda: os.system("git init"))


def add (file, gitdir="."):

    return dolocal (gitdir, lambda: os.system("git add " + file))


def clone (remote):

    """
    Do a clone of a repo
    remote string holds all of the git info
    """
    if (remote==""):
        return
    return os.system("git clone " + remote)


def cloneto (gitremote, gitlocaldir):

    """
    This pulls down a remote git tree to a specific local directory
    """
    if (gitremote==""):
        return
    createdir(gitlocaldir)
    return dolocal (gitlocaldir, lambda: git.clone(gitremote))


def commit (gitdir, comment):

    return dolocal (gitdir, lambda: os.system('git commit -m "' + comment + '"'))


def createbranch (gitdir, branch):

    return dolocal (gitdir, lambda: os.system('git branch ' + branch))


def switchbranch (gitdir, branch):
    
    return dolocal (gitdir, lambda: os.system('git checkout ' + branch))

