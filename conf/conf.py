#!/usr/bin/env python

from conflib import *

import argparse



## When run directly do the full link files, need to specify the git conf directory
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Confset: A distributed configuration management tool")

    parser.add_argument('verb', choices=['update', 'refresh', 'new', 'install', 'local', 'help'], default='')
    
    parser.add_argument('--set', default="")

    args = parser.parse_args()
    
    try:
        noun = args.set
    except NameError:
        noun = ''

    if args.verb == "update":
        info ("Updating")
        conf_update(noun)

    elif args.verb=="refresh":
        info ("Refreshing")
        conf_refresh(noun)

    elif args.verb=="new":
        info("Initialising")
        conf_new(noun)

    elif args.verb=="install":
        info("Initialising")
        conf_install(noun)

    elif args.verb=="init":
        error("Local init setup not supported yet")
