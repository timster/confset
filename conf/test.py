import unittest
import defaults
import random
import shutil
import os
import fs 
import defaults
import conf
import util
import finddots
import git

## Turn on harmful effects and logging off
fs.harm = True
util.loglevel = -1

class TestConfSet(unittest.TestCase):

    basedir = '/tmp/conftest/'
    testconfset = {'basedir': basedir,
         
                   'confs' : [
                       '.arc', '.brc', '.crc', '.drc'
                   ],
               
                   'gitdir': basedir + 'git/git/etc',
                   'savedir': basedir + 'oldconfig', 
                   
                   'gitremote': "https://timster@bitbucket.org/timster/conf.git",
                   'harm': True,
                   'loglevel': -1,
    }

    def setUp(self):

        ##print("Building confset files")
        self.makefiles(50);
        
        
    def tearDown(self):

        ##print("Clearing confset files")
        shutil.rmtree(self.testconfset['basedir'])
        
    @staticmethod
    def genrcs(num, basename):

        ## Generate a set of filenames
        def genfilename(num):

            return ('.' + basename + str(num) + 'rc')

        files = map(genfilename, range(0,num))
        return files


        
    def makefiles(self, size=50):
        """
        Make a test confset with size files ready to be linked up
        """
        #files = self.testconfset['confs']
        basedir = self.testconfset['basedir']
        gitdir = self.testconfset['gitdir'] + "/"

        if os.path.exists(basedir):
        
            shutil.rmtree(basedir)
        
        fs.createdir(basedir)
        fs.createdir(gitdir)

        git.init(gitdir)
        git.add("-u", gitdir)
        files = self.genrcs(size, "orig")
        
        for f in files:
            fs.touch (gitdir + f)

        self.testconfset['confs'] = files
        # Make some existing files in the basedir
        size = random.randrange(0,len(files))

        for f in files[0:size]:
            fs.touch (basedir + f)

        ## Drop the config file into the git tree
        defaults.spitdefaults(self.testconfset, gitdir, name="defaults.json")
        
            
    ## Now the tests:
            
    def test_createconf(self):

        conf.conf_reconfig_defs(self.testconfset, stash=False, forcelink=True)

        files = self.testconfset['confs']
        basedir = self.testconfset['basedir']
        gitdir = self.testconfset['gitdir'] + "/"
        savedir = self.testconfset['savedir']
        
        #basefiles = set(fs.lsdots(basedir))
        basefiles = set(finddots.getdots(basedir))
        gitfiles = set(fs.lsdots(gitdir))
        savedfiles = set(fs.lsdots(savedir))

        #print (basefiles)
        #print (gitfiles)
        #print (savedfiles)

        all_linked = (basefiles == gitfiles)

        saved_subset = savedfiles.issubset(gitfiles)
        
        self.assertTrue(all_linked and saved_subset)


    def test_updateconf(self):

        conf.conf_reconfig_defs(self.testconfset, stash=False, forcelink=True)
        gitdir = self.testconfset['gitdir'] + "/"
       
        defs = defaults.loadconfsetconfig(dir=gitdir)

        news = 10;
        orig_size = len(defs['confs'])
        
        ## Now add some files to the confset
        newfiles = self.genrcs(news, "new")
        defs['confs'].extend(newfiles)
        ## ...and the git tree
        for f in newfiles:
            fs.touch (gitdir + f)

        ## And drop the new conf file
        defaults.spitdefaults(defs, gitdir, name="defaults.json")
            
        conf.conf_reconfig_defs(self.testconfset, stash=False, forcelink=False)
        ## Check all of the new files are linked
        basedir = self.testconfset['basedir']
        gitdir = self.testconfset['gitdir'] + "/"
        
        basefiles = set(fs.lsdots(basedir))
        gitfiles = set(fs.lsdots(gitdir))
        
        all_linked = (basefiles == gitfiles)

        right_size = (len(gitfiles) - news) == orig_size
        
        self.assertTrue(all_linked and right_size)

    def test_stash(self):

        conf.conf_reconfig_defs(self.testconfset, stash=True, forcelink=True)
        self.assertTrue(True)


        
if __name__ == '__main__':

    #fs.harm = False
    #unittest.main()
    fs.harm = True
    unittest.main()
