# Holds the local setup
#import os
import json
import fs

#from os.path import expanduser
from util import *

# Default to home directory - rarely need anything else for user usage
basedir = fs.homedir() ##expanduser("~")+ '/'


## This is a dict object now
## This is the default values for it, legacy now probably
defaultconfset = {'basedir': basedir,
         
           # These are all of the configs I want to save
           'confs' : [
               '.bashrc', '.fvwm2rc', '.i3', '.i3status.conf', 
               '.xemacs', '.xinitrc', 
               ['.emacs', '.xemacs/init.el']
           ],
           
           'gitdir': basedir + 'git/git/etc',
           'savedir': basedir + 'oldconfig',  #For preserving existing conf files

           ## This is for the current setup 
           'gitremote': "https://timster@bitbucket.org/timster/conf.git",


           # These are the potential confs I do *not* want to save
           'drops': ['ssh', 'sudo', 'fsm', 'alienFXLock', 'pulse', 'lock', 
                      'ICEauthority', 'esd_auth', 'serverauth', '~', 'save',
                      'goutputstream', 'errors', 'dbus', 'history',
                      'fonts', 'thumbnails', 'Trash', '.old',
                      'cache', ]
       }

def spitdefaults (confs, dir=".", name="def.json"):
    """
    Spits the conf set to a JSON file in dir
    """
    with open(os.path.join(dir, name), "w") as the_file:
        json.dump(confs, the_file)
        the_file.close()


def loadjson(filename, dir):
    """
    Loads the json file into a dict object
    """
    full = os.path.join(dir, filename)
    try:
        with open(full, "r") as the_file:
            c = json.load(the_file)
            info ("Loading JSON object from: " + full)
            the_file.close()
            return c
    except IOError as e:
        error("Can't load config file " + dir + "/" + filename)
        raise

def loadconfsetconfig (filename="defaults.json", dir="."):
    """
    Loads the confset config to a file in dir
    """
    info ("Loading confset config file '" + filename + "' from directory '" + dir + "'")

    c = loadjson(filename, dir)
    
    ## Resolve basedir 
    if c['basedir'] == '~':
        c['basedir'] = fs.homedir()
    return c



def cleanconfdef (confdef):

    if confdef['basedir']=='~':
        confdef['basedir'] = basedir
 
        
    confdef['gitdir'] = basedir + "/" + confdef['gitdir']
    confdef['savedir'] = basedir + "/" + confdef['savedir']
    return confdef


def loadconfdef (confdef=""):
    
    """
    This loads the user defined .confrc found in the home directory
    For each confset this provides:
            1) a short name
            2) A location of the git repository (online)
            3) The local directory where the git project resides
            4) Optional local config including the configuration files you want excluded 
    """

    info ("Using homedir=" + fs.homedir())
    defs = loadjson(".confrc", fs.homedir())

    if confdef=="": ## Return the first by default
        return cleanconfdef(defs[defs.keys()[0]])
    else:
        try: 
            return cleanconfdef(defs[confdef])
        except KeyError:
            fatal ("Confset '" + confdef + "' not defined") 
            return

        


