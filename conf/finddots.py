import os
import glob
import string


from defaults import *
from util import *

## This should match most dotfiles, for now
## Note dot filename shorter than three chars won't match
## Add: match all and remove '.' and '..' entries 
def globdots ():
    return glob.glob(".???*")


## The aim here is to try to locate conf files directly
def getdots (dir):
    return dolocal (dir, globdots)

def trackedfiles (gitdir): 
   return getdots (gitdir)


def finddotfiles (confset): 
    "Discover new dotfiles apart from the ones we already have in our existing confset"
    basedir = confset['basedir']
    gitdir = confset['gitdir']
    drops = confset['drops']

    all = getdots(basedir)

    ## Remove those that we are already tracking
    tracked = trackedfiles(gitdir)

    #Do a diff
    d = diff(all, tracked)
    d = dropmatches (d, drops)
    return d
