import os
import glob
import json
import sys



## Obsolete now that we read json data directly,
## We have removed allowing python code in config file
"""
def include(filename):
    "Execute the python code in filename if it is there"
    if os.path.exists(filename): 
        execfile(filename)
"""

## Change to a directory run command and back out
def dolocal (dir, func):
    
    here = os.getcwd()
    os.chdir (dir)
    ret = func()
    os.chdir(here)
    return ret

def ls (dir = "."):

    dolocal (dir, lambda: os.system("ls"))


## The lambda creates a local function of no variables
def cleanconftree (dir):
    """
    Removes all of the ~ files in the tree dir
    """
    dolocal(dir,  lambda: os.system ("find . -name \"*~\" -exec rm {} \;"))


def substring (big, small):
    "Does big string contain string small?"
    return string.find(big, small) > 0

## Set difference
def diff(a, b):
    b = set(b)
    return [aa for aa in a if aa not in b]

def dropmatch(lst, drop):
    "Drop all strings in lst that contain drop string"
    return [aa for aa in lst if not substring(aa, drop)]

def dropmatches (lst, drops):
    """
    This removes all of the patterns in drops from list lst
    """
    rem = lst
    for d in drops:
        rem = dropmatch(rem, d)
    return rem

loglevel = 2

## Logging stuff, 
def log (num, mess):
    """
    Log to stdout the message at the appropriate level
    Higher numbers request more logging
    """
    if (num <= loglevel):
        print mess

def error (mess):
    log(1, "Error: " + mess)

def warn (mess):
    log(2, "Warning: " + mess)

def change (mess):
    log(3, "Change: " + mess)

def info (mess):
    log(4, "Info: " + mess)

def fatal (mess):
    log(0, "Fatal: " + mess)
    sys.exit(1)





