## Wrapper for potentially destructive filesystem access 
from util import *
import os
import shutil

harm = False

def move (fromm, to):
    "Move file fromm to"
    if harm:
    
        ## This should create all required intermediate paths
        change("Moving file " + fromm + " => " + to)
        os.renames(fromm, to)
    else:
        info("'Move' file " + fromm + " => " + to)

def rm (file):
    "Remove file"
    if harm:
        os.remove(file)
    else:
        info("'Deleting' file " + fromm + " => " + to)

def symlink (fromm, to):
    "Symlink file"
    if harm:
    
        change("Symlink file " + fromm + " => " + to)
        parent = os.path.dirname(to)
        if not os.path.exists(parent):
            os.makedirs(parent) ##Create all ancestor directories
        os.symlink(fromm, to)
    else:
        info("'Symlink' file " + fromm + " => " + to)


def createdir (dir):
    "Create directory, if it does not exist"
    if not os.path.exists(dir):

        if harm:
            os.makedirs(dir) 
            change ("Creating directory:" + dir)
        else:
            info ("'Creating' directory:" + dir)


def touch(fname):
    fhandle = open(fname, 'a')
    try:
        os.utime(fname, None)
    finally:
        fhandle.close()

def isdotfile(name):

    ## Does it start with a dot?
    return name[0]=='.'
    
    
def ls(path):

    return os.listdir(path)


def lsdots(path):

    files = ls(path)
    dots = filter(isdotfile, files)
    if ('.git' in dots):
        dots.remove('.git')
    return dots


def homedir():
    dir = os.path.expanduser("~")+ '/'
    return dir
