import os

from util import *
from defaults import *

import fs


def linkfile (file, savedir, basedir, gitdir, target="", force=False):
    """
    Usage: linkfile name gitdirectory
    preserves and links in config files from etc
    Need to handle directories too
    Without 'force' it is not destructive to existing files/links
    """
    fs.createdir (savedir)

    ##Check for existing link 
    if os.path.islink(basedir + "/" + file):

        if (force):
            fs.rm(basedir + "/" + file)
            change(file + "=> removing existing link")

        else:
            info(file + "=> link already exists, ignoring")
            return

    ##Check for existing file
    ##Preserve original file, if necessary
    ## Add: use git branch/merge to handle this
    ## Need to allow for directories here, too
    if os.path.isfile(basedir + "/" + file) or os.path.isdir(basedir + "/" + file):
	change(file + "=> file already exists, preserving")
	fs.move(basedir + "/" + file,  savedir +"/" + file)

    ##Link in the new config file
    change (file + " => Linking in config file")

    if target == "":
        fs.symlink(os.path.normpath(gitdir + "/" + file), os.path.normpath(basedir + "/" + file))
    else:
        change("Linking " + file + " => "  + target) 
        fs.symlink(os.path.normpath(gitdir + "/" + target), os.path.normpath(basedir + "/" + file))


def linkall (defs, confset, force = False):
    """
    Link all of the confs in the confset across
    """
    gitdir = defs['gitdir']
    savedir = defs['savedir']
    confs = confset['confs']
    basedir = confset['basedir']

    for f in confs:
        if len (f)==2: #If we have defined the target explicitly...
            linkfile (f[0], savedir, basedir, gitdir, f[1], force)
        else: ## Use the same file name
            linkfile (f, savedir, basedir, gitdir, "", force)
