import os
import git

import fs
from util import *


## Need to allow for different targets
def tostash (file, gitdir, basedir, target=""):
    """
    Take an existing conf file in basedir and put it under git
    """
    if target=="":
        target = file

    # If file or directory is already under git abort
    if os.path.isfile(gitdir+"/"+file) or os.path.isdir(gitdir + "/" + file):
	info (file + ": Already in git tree, aborting")
	return

    # If the source file is there
    if (os.path.isfile(basedir + "/" + file) or os.path.isdir(basedir + "/" + file)): 
	change(file + ": Adding to git conf")	

        fs.move (basedir + "/" + target,  gitdir + "/" + file)

        # This is recursive adding whole tree where it is a directory
        git.add(file, gitdir)

    else:
	error (file + ": No such file ")

def stashall (defs, confset, commit = True, comment="Automatic commit from confset"):
    """
    Push all in confset to the git tree
    """
    confs = confset['confs']
    basedir = confset['basedir']
    gitdir = defs['gitdir']
    
    ## Check that the git tree is initialised
    change("Stashing from " + basedir + " to " + gitdir) 

    for f in confs:
        if len (f)==2: #If we have defined the target explicitly...
            tostash (f[0], gitdir, basedir, target = f[1])
        else: ## Use the same file name
            tostash (f, gitdir, basedir)
    if commit:
        git.commit(gitdir, comment)
