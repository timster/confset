import os
import shutil
import glob
import string

from finddots import finddotfiles
from link import *
from stash import *

import util

import git

    
def conf_reconfig_defs (defs, stash, forcelink):

    """
    Work to refresh the supplied conf defs
    """

    gitdir = defs['gitdir']
    
    ## Load the defaults in the gitdir
    confset = loadconfsetconfig("defaults.json", gitdir)

    ##fs.harm = confset['harm']
    ## Get the harm switch set from the top level config
    fs.harm = defs['harm']

    if fs.harm:
        warn("Enabling harmful file ops")
    else:
        info("Running in safe mode :)")

    util.loglevel = defs['loglevel']
    

    ## Put all confs under git and link
    if stash:
        stashall (defs, confset)
    linkall(defs, confset, force=forcelink)


def conf_reconfig (name, stash, forcelink):

    """
    Runs over the confset name 
    stash tells whether to put existing conf files into the tree
    forcelink tells if we replace existing symlinked confs with git tree ones
    """

    defs = loadconfdef(name)
    conf_reconfig_defs (defs, stash, forcelink)


def conf_refresh(name=''):
    """
    This loads the defaults and checks that your links are up to date
    Assumes that it has already been installed. Preserves local setup
    as it does not touch exising links and does not stash fresh files.
    """
    conf_reconfig(name, stash=False, forcelink=False)


def conf_new (name=""):
    """
    Scenario: 
    You have already cloned the git tree and want to link up
    There may be existing links - so force a breakage of them
    """
    conf_reconfig(name, stash=False, forcelink=True)
    

def conf_update (name=""):
    """
    Scenario: 
    You have added files to the defaults.json confset and want to 
    bring your local setup in line. Existing links will be preserved. 
    """
    conf_reconfig(name, stash=True, forcelink=False)


def conf_install (name=""):
    """
    Scenario: 
    Download and install a remote git conf set
    Assumes that the set name is set up in .confrc
    """
    defs = loadconfdef(name)
    
    gitdist = defs['gitremote']
    localdir = defs['gitdir']
    
    ## Pull remote git conf down to local directory
    git.cloneto(gitdist, localdir)
    
    ## Read in the defaults from the new conf set
    confset = loadconfsetconfig("defaults.json", localdir)

    newconf(name)
    
    ## Now link the files across
    conf_new(name)


def conf_local (confset, find=False):

    """
    Scenario:
    You have just started and want to build a confset
    Create a new git tree and put all of the local files in conf under it
    """
    localdir = confset['gitdir']

    ##Create local git tree if it does not exist
    git.init(localdir)

    if find:
        ##Build a list of conf files
        cnfs = finddotfiles()
        confset['confs'] = cnfs ## Update confset to reflect this
    else:
        cnfs = confset['confs']

    ## Bring them all across
    stashall(confset)

    ## Add: now generate a defaults.json in the correct place for editing
    
